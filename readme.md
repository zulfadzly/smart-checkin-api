Development setup

1. Rename the docker-compose-dev.yml.bak to docker-compose-dev.yml

2. Update the docker-compose-dev.yml as needed

3. Rename client_secrets.json.bak to client_secrets.json

4. Update your keycloak information

5. Start the docker container
    docker-compose -f docker-compose-dev.yml  up -d --remove-orphans
    
6. Install smartCheckin db

    docker exec -it smart-checkin-api python3 manage.py db init

    docker exec -it smart-checkin-api python3 manage.py db migrate --message 'initial database migration'

    docker exec -it smart-checkin-api python3 manage.py db upgrade

7.Browse http://localhost:5002/api/1/

8. Stop the docker containers by
    docker-compose -f docker-compose-dev.yml  down --remove-orphans


Deployment

1. Rename the docker-compose-dev.yml.bak to docker-compose.yml

2. Update the docker-compose.yml as needed

3. Rename client_secrets.json.bak to client_secrets.json

4. Update your keycloak information

5. Start the docker container
    docker-compose up -d --remove-orphans

6. Stop the docker containers by
    docker-compose down --remove-orphans

Import DB

docker exec -i smart-checkin-api-mysql mysql -uroot -pmys3cretp@5w0Rd smartCheckin < smartCheckin.sql

Connect to DB

docker exec  -it smart-checkin-api-mysql mysql -uroot -pmys3cretp@5w0Rd

View table structure

DESC customers;