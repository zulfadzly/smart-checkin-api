FROM centos:8

MAINTAINER Zulfadzly <mzulfadzly@yahoo.com>

ADD . /smartCheckin

RUN yum -y update && \
    yum -y install \
	telnet \
	python36 \
	python3-devel \
	gcc-c++ \
    wget && \
    yum install -y /smartCheckin/packages/mysql-devel-8.0.21-1.module_el8.2.0+493+63b41e36.x86_64.rpm \
    yum remove -y --skip-broken --nobest \
    python3-devel \
    gcc-c++ && \
    curl https://bootstrap.pypa.io/get-pip.py | python3.6 && \
    yes | pip3 install -r /smartCheckin/requirements.txt && \
    yum clean all && \
    rm -rf /var/cache/yum

WORKDIR /smartCheckin

EXPOSE 5002

CMD ["python3", "-u", "manage.py", "run"]