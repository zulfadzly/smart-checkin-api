import logging, sys
from flask import request, g
from flask_restx import Resource,  Namespace, fields, abort
from functools import wraps
from ..config import token

from ..util.dto import CustomerDto
from ..service.customer_service import get_all_customer ,save_new_customer ,delete_customer ,get_customer \
    ,update_customer
from .. import oidc

api = CustomerDto.api
_customer = CustomerDto.customer
_createCustomer = CustomerDto.createCustomer
_updateCustomer = CustomerDto.updateCustomer
_deleteCustomer = CustomerDto.deleteCustomer


logging_formatter = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(filename='/smartCheckin/app/logs/app.log', level=logging.DEBUG, format=logging_formatter)
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger('customerFeedback').addHandler(console)
logger = logging.getLogger(__name__)


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwagrs):

        request_token = None

        if 'X-API-KEY' in request.headers:
            request_token = request.headers['X-API-KEY']

        if not token:
            return {'message': 'Token is missing.'}, 401

        if request_token != token:
            return {'message': 'wrong token passed.'}, 401

        return f(*args, ** kwagrs)

    return decorated


@api.route('/customer')
class CustomerList(Resource):
    @oidc.require_login
    @oidc.accept_token(require_token=True, scopes_required=['openid'])
    @api.doc('customer_list')
    @api.marshal_list_with(_customer, envelope='data')
    def get(self):
        auth_data = oidc.user_getinfo(['sub', 'user_type'])
        """ List all customer """
        r = request
        logger.info('%s %s', 'List all customer', r)
        return get_all_customer(auth_data, g.oidc_token_info)

    @api.response(200, 'Customer successfully created.')
    @oidc.require_login
    @oidc.accept_token(require_token=True, scopes_required=['openid'])
    @api.doc('create_new_customer')
    @api.expect(_createCustomer, validate=False)
    def post(self):
        auth_data = oidc.user_getinfo(['sub', 'user_type'])
        """ Creates a new customer """
        data = request.json
        logger.info('%s %s', 'Creates a new customer', data)
        return save_new_customer(data, auth_data, g.oidc_token_info)


@api.route('/customer/<int:id>')
@api.param('id', 'The customer identifier')
@api.response(200, 'Success')
@api.response(404, 'Failed')
class GetTodo(Resource):
    @api.doc(security='apiKey')
    @token_required
    @api.doc('delete_customer')
    def delete(self, id):
        """ Delete customer """
        logger.info('%s %s', 'Delete customer #', id)
        return delete_customer(id)

    @api.doc(security='apiKey')
    @token_required
    @api.doc('get_customer')
    @api.marshal_list_with(_customer,  envelope='data')
    def get(self, id):
        """ Get customer """
        logger.info('%s %s', 'Get customer #', id)
        return get_customer(id)

    @api.doc(security='apiKey')
    @token_required
    @api.doc('update_customer')
    @api.expect(_updateCustomer, validate=True)
    def put(self, id):
        """ Update customer """
        data = request.json
        logger.info('%s %s %s', 'Update customer #', id, data)
        return update_customer(id, data=data)

    # @api.doc(security='apiKey')
    # @token_required
    # @api.doc('update_todo_status')
    # @api.expect(_updateTodoStatus, validate=True)
    # def patch(self, id):
    #     """ Update todo status """
    #     data = request.json
    #     logger.info('%s %s %s', 'Update todo status #', id, data)
    #     return update_todo_status(id, data=data)
