import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config:

    DEBUG = True
    SECRET_KEY = os.getenv('SECRET_KEY', 'r@!n |N 5p@In f@lls m@!nlY 0n Th3 pl@!N')
    TOKEN = 'aDiRieW4wFt0EpehvsuJdTjK8c1KnRBb'
    DEFAULT_CUSTOMER_SHOW_STATUS = 1
    DEFAULT_PARTNER_ID = 1
    CUSTOMER_DELETE_STATUS = 2
    DEFAULT_CUSTOMER_SYSTEM_STATUS = 1
    DEFAULT_SUPER_ADMIN_USER_TYPE = 'Super Admin'
    DEFAULT_PARTNER_USER_TYPE = 'Partner'
    DEFAULT_PARTNER_ROLES = 'Partner'

    BLOCKED_CUSTOMER_UPDATE_KEY = [
        'partner_id'
        'create_date',
        'id',
        'system_status',
        'show_status',
        'create_date',
        'update_date'
    ]
    TESTING = True
    OIDC_CLIENT_SECRETS = 'client_secrets.json'
    OIDC_SCOPES = ['openid', 'email', 'profile', 'roles']
    OIDC_INTROSPECTION_AUTH_METHOD = 'client_secret_post'
    OIDC_TOKEN_TYPE_HINT = 'access_token'
    OIDC_ID_TOKEN_COOKIE_SECURE = False
    OIDC_REQUIRE_VERIFIED_EMAIL = False
    OIDC_USER_INFO_ENABLED = True
    OIDC_OPENID_REALM = 'smart-checkin-api'
    SWAGGER_UI_OAUTH_CLIENT_ID = 'smart-checkin-api-web-user'
    SWAGGER_UI_OAUTH_REALM = 'smart-checkin-api'
    SWAGGER_UI_OAUTH_APP_NAME = 'smart-checkin-api'

class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_DATABASE_URI = 'mysql://' + os.getenv('DB_USER') + ':' + os.getenv('DB_PASSWORD') + '@' + \
                              os.getenv('DB_HOST') + '/' + os.getenv('DB')

class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'todo.db')
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = True

class QAConfig(Config):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_DATABASE_URI = 'mysql://' + os.getenv('DB_USER') + ':' + os.getenv('DB_PASSWORD') + '@' + \
                              os.getenv('DB_HOST') + '/' + os.getenv('DB')

class StagingConfig(Config):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_DATABASE_URI = 'mysql://' + os.getenv('DB_USER') + ':' + os.getenv('DB_PASSWORD') + '@' + \
                              os.getenv('DB_HOST') + '/' + os.getenv('DB')

class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'mysql://' + os.getenv('DB_USER') + ':' + os.getenv('DB_PASSWORD') + '@' + \
                              os.getenv('DB_HOST') + '/' + os.getenv('DB')


config_by_name = dict(
    dev=DevelopmentConfig,
    test=TestingConfig,
    staging=StagingConfig,
    qa=QAConfig,
    prod=ProductionConfig
)
token = Config.TOKEN
default_customer_show_status = Config.DEFAULT_CUSTOMER_SHOW_STATUS
default_customer_system_status = Config.DEFAULT_CUSTOMER_SYSTEM_STATUS
customer_delete_status = Config.CUSTOMER_DELETE_STATUS
blocked_customer_update_key = Config.BLOCKED_CUSTOMER_UPDATE_KEY
default_partner_id = Config.DEFAULT_PARTNER_ID
default_super_admin_user_type = Config.DEFAULT_SUPER_ADMIN_USER_TYPE
default_partner_user_type = Config.DEFAULT_PARTNER_USER_TYPE
default_partner_roles = Config.DEFAULT_PARTNER_ROLES
