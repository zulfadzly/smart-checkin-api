from flask_restx import Namespace, fields


class CustomerDto:

    api = Namespace('customer', description='customer related operations')

    customer = api.model('customer', {
        'id': fields.Integer,
        'partner_id': fields.String,
        'company_name': fields.String,
        'company_address': fields.String,
        'company_postcode': fields.String,
        'company_country': fields.String,
        'company_state': fields.String,
        'company_district': fields.String,
        'show_status': fields.Integer,
        'system_status': fields.Integer,
        'create_date': fields.String,
        'update_date': fields.String
    })

    createCustomer = api.model('createCustomer', {
        'company_name': fields.String,
        'company_address': fields.String,
        'company_postcode': fields.String,
        'company_country': fields.String,
        'company_state': fields.String,
        'company_district': fields.String
    })

    updateCustomer = api.model('updateCustomer', {
        'company_name': fields.String,
        'company_address': fields.String,
        'company_postcode': fields.String,
        'company_country': fields.String,
        'company_state': fields.String,
        'company_district': fields.String
    })

    deleteCustomer = api.model('deleteCustomer', {
        'id': fields.Integer
    })