from .. import db, ma
from app.main.model.customers import Customer


class CustomerSchema(ma.Schema):
    class Meta:
        model = Customer,
        fields = (
            'id',
            'partner_id',
            'company_name',
            'company_address',
            'company_postcode',
            'company_country',
            'company_state',
            'company_district',
            'show_status',
            'system_status',
            'create_date',
            'update_date'
        )


new_customer_schema = CustomerSchema()
update_customer_schema = CustomerSchema()