import uuid
import datetime
from app.main import db
from app.main.model.customers import Customer
from app.main.schema.schema import new_customer_schema, update_customer_schema
from ..config import default_customer_show_status, default_customer_system_status, customer_delete_status \
    , blocked_customer_update_key, default_partner_id, default_super_admin_user_type, default_partner_user_type
from sqlalchemy.exc import SQLAlchemyError
import logging
import sys

logging_formatter = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(filename='/smartCheckin/app/logs/app.log', level=logging.DEBUG, format=logging_formatter)
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger().addHandler(console)
logger = logging.getLogger(__name__)


def get_all_customer(auth_data, token_data):
    user_type = auth_data['user_type']
    uid = auth_data['sub']
    if user_type == default_super_admin_user_type:
        logger.info('%s %s', 'Super Admin starting querying companies uid # ', uid)
        return db.session\
            .query(Customer)\
            .all()
    elif user_type == default_partner_user_type:
        logger.info('%s %s', 'Partner starting querying companies uid # ', uid)
        return db.session\
            .query(Customer) \
            .filter(Customer.partner_id == uid) \
            .all()
    else:
        response_object = {
            'message': 'Insufficient permission',
            'status': False,
        }
        logger.error('%s %s', 'Insufficient permission uid # ', uid)
        return response_object, 404


def save_new_customer(data, auth_data, token_data):
    user_type = auth_data['user_type']
    uid = auth_data['sub']
    if user_type == default_super_admin_user_type or user_type == default_partner_user_type:
        try:
            partner_id = uid
            show_status = default_customer_show_status
            system_status = default_customer_system_status
            new_customer = Customer(
                partner_id=partner_id,
                company_name=data['company_name'],
                company_address=data['company_address'],
                company_postcode=data['company_postcode'],
                company_country=data['company_country'],
                company_state=data['company_state'],
                company_district=data['company_district'],
                show_status=show_status,
                system_status=system_status,
                create_date=datetime.datetime.utcnow()
            )
            db.session.add(new_customer)
            db.session.commit()
            new_customer_data = new_customer_schema.dump(new_customer)
            response_object = {
                'message': 'Customer successfully created.',
                'status': True,
                'data': new_customer_data,
            }
            logger.info('%s %s', 'successfully created customer #', new_customer.id)
            return response_object, 200
        except SQLAlchemyError as e:
            logger.error('%s %s', 'failed to create customer error', e)
            db.session.rollback()
            db.session.flush()
            response_object = {
                'message': 'Failed to create customer',
                'status': False,
            }
            return response_object, 404
    else:
        response_object = {
            'message': 'Insufficient permission',
            'status': False,
        }
        logger.error('%s %s', 'Insufficient permission uid # ', uid)
        return response_object, 404


def delete_customer(customer_id):
    # TODO : super admin can delete all customer and partner admin can only delete its own customer
    customer_exist = len(Customer.query.filter_by(id=customer_id).all())
    if customer_exist == 1:
        customer = Customer.query.filter_by(id=customer_id).first()
        # TODO : cascade operation
        customer.update_date = datetime.datetime.utcnow()
        customer.show_status = customer_delete_status
        db.session.commit()
        response_object = {
            'message': 'Customer delete successfully',
            'status': True,
        }
        logger.info('%s %s', 'successfully deleted customer #', customer.id)
        return response_object, 200
    else:
        response_object = {
            'message': 'Failed to delete customer',
            'status': False,
        }
        logger.error('%s %s', 'failed to delete customer #', customer_id)
        return response_object, 404


def get_customer(customer_id):
    # TODO : filter by partner admin or super admin using keycloak session
    # TODO : super admin will be able to see all customer and all statuses other will only their own customer with active status
    customer = Customer.query.filter_by(id=customer_id).first()
    if customer is None:
        return False
    else:
        return customer


def update_customer(customer_id, data):
    # TODO : filter by partner admin or super admin using keycloak session
    customer_exist = len(Customer.query.filter_by(id=customer_id).all())
    if customer_exist == 1:
        customer = Customer.query.filter_by(id=customer_id).first()
        for key, value in data.items():
            if key not in blocked_customer_update_key:
                setattr(customer, key, value)
        customer.update_date = datetime.datetime.utcnow()
        db.session.commit()
        update_customer_data = update_customer_schema.dump(customer)
        response_object = {
            'message': 'Customer updated successfully',
            'status': True,
            'data': update_customer_data,
        }
        logger.info('%s %s', 'successfully updated customer #', customer.id)
        return response_object, 200
    else:
        response_object = {
            'message': 'Failed to update customer',
            'status': False,
        }
        logger.error('%s %s', 'failed to update customer #', customer_id)
        return response_object, 404
