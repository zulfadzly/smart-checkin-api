from .. import db
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import text

Base = declarative_base()


class Customer(db.Model, Base):

    """ Customer Model for storing customer related details """
    __tablename__ = "customers"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    partner_id = db.Column(db.String(255), nullable=False)
    company_name = db.Column(db.String(255), nullable=False)
    company_address = db.Column(db.String(255), nullable=False)
    company_postcode = db.Column(db.String(255), nullable=False)
    company_country = db.Column(db.String(10), nullable=False)
    company_state = db.Column(db.String(10), nullable=False)
    company_district = db.Column(db.String(10), nullable=False)
    show_status = db.Column(db.Integer, nullable=False, server_default=text('0'))
    system_status = db.Column(db.Integer, nullable=False, server_default=text('0'))
    create_date = db.Column(db.DateTime, nullable=False)
    update_date = db.Column(db.DateTime, nullable=True)

    def __init__(
            self,
            partner_id,
            company_name,
            company_address,
            company_postcode,
            company_country,
            company_state,
            company_district,
            show_status,
            system_status,
            create_date
    ):
        self.partner_id = partner_id
        self.company_name = company_name
        self.company_address = company_address
        self.company_postcode = company_postcode
        self.company_country = company_country
        self.company_state = company_state
        self.company_district = company_district
        self.show_status = show_status
        self.system_status = system_status
        self.create_date = create_date