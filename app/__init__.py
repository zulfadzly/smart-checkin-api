import os
from flask_restx import Api
from flask import Blueprint
from dotenv import load_dotenv
from .main.controller.smart_checkin_controller import api as smart_checkin_ns

load_dotenv()

blueprint = Blueprint('api', __name__)

authorizations = {
    'apiKey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'X-API-KEY'
    }
}


if os.getenv('API_ENV') == 'prod' or os.getenv('API_ENV') == 'staging':
    api = Api(blueprint,
              title='Smart Checkin',
              version='1.0',
              description='Smart Checkin API',
              authorizations=authorizations,
              doc=False
              )
else:
    api = Api(blueprint,
              title='Smart Checkin',
              version='1.0',
              description='Smart Checkin API',
              security={'OAuth2': ['read', 'write']},
              authorizations={
                  'OAuth2': {
                      'type': 'oauth2',
                      'flow': 'implicit',
                      'authorizationUrl': 'http://192.168.0.107:8080/auth/realms/smart-checkin-api/protocol/openid-connect/auth',
                      'clientId': 'smart-checkin-api-web-user',
                      'scopes': {
                          'openid': 'Get ID token',
                          'profile': 'Get identity',
                      },
                      'additionalQueryStringParams': {
                          'nonce': '325qjlalf09230'
                      }
                  }
                }
              )


api.add_namespace(smart_checkin_ns, path='/smartCheckin')